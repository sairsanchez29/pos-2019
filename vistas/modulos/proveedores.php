<?php


/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

if($_SESSION["perfil"] == "Vendedor"){

  echo '<script>

    window.location = "./";

  </script>';

  return;

}

?>
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar proveedores
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="./"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar proveedores</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarProveedor">
          
          Agregar proveedor

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
            <th>Nit</th>
           <th>Proveedor</th>
           <th>Telefono</th>
           <th>Dirección</th>
           <th>Correo</th>
           <th>Página web</th>
           <th>Stock asignado</th>
           <th>Última asignación de stock</th>
           <th>Acciones</th>
         </tr> 

        </thead>

        <tbody>

        <?php

        $item = null;
        $valor = null;

        $proveedores = ControladorProveedores::ctrMostrarProveedores($item, $valor);

       foreach ($proveedores as $key => $value){
         
        $CompraProductos = ControladorProveedores::ctrMostrarProductosComprados("id_proveedor" , $value["id"]);
        $NproductosComprados = 0;
        foreach ($CompraProductos as $cp) {
          $NproductosComprados = $NproductosComprados + 1 ;
        }
        if ($NproductosComprados == 0) { $fecha = ""; }else{$fecha = $cp["fecha"]; }

          echo '<tr>
                  <td>'.($key+1).'</td>
                  <td>'.$value["nit"].'</td>
                  <td>'.$value["proveedor"].'</td>
                  <td>'.$value["telefono"].'</td>
                  <td>'.$value["direccion"].'</td>
                  <td>'.$value["correo"].'</td>
                  <td>'.$value["pagina_web"].'</td>
                  <td><a href="index.php?ruta=HistorialProveedor&key='.$value["id"].'"  class="btn btn-default">'.$NproductosComprados.' </a></td>
                  <td>'.$fecha. '</td>
                  
                    <td>

                    <div class="btn-group">
                        
                      <button class="btn btn-warning btnEditarProveedor" idProveedor="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarProveedor"><i class="fa fa-edit"></i></button>

                      <button class="btn btn-danger btnEliminarProveedor" idProveedor="'.$value["id"].'" ><i class="fa fa-times"></i></button>

                    </div>  

                  </td>
                </tr>';


                
        }


        ?> 

        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL AGREGAR USUARIO
======================================-->

<div id="modalAgregarProveedor" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" >

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar proveedor</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

             <!-- ENTRADA PARA EL NIT-->

             <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-file-contract"></i></span> 

                <input type="number" class="form-control input-lg" name="nuevoNit" placeholder="Ingresar NIT del proveedor" id="nuevoNit" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevoProveedor" placeholder="Ingresar nombre del proveedor" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL TELEFONO-->

             <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 

                <input type="number" class="form-control input-lg" name="nuevoTelefono" placeholder="Ingresar telefono" id="nuevoTelefono" required>

              </div>

            </div>

             <!-- ENTRADA PARA LA DIRECCIÓN-->

             <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-map-marked-alt"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevaDireccion" placeholder="Ingresar la dirección " required>

              </div>

            </div>

            <!-- ENTRADA PARA EL CORREO -->

             <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-at"></i></span> 

                <input type="email" class="form-control input-lg" name="nuevoCorreo" placeholder="Ingresar el correo (opcional)">

              </div>

            </div>


              <!-- ENTRADA PARA LA PÁGINA WEB-->

             <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-globe"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevaPaginaWeb" placeholder="Ingresar la página web (opcional)">

              </div>

            </div>


           

            

          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar proveedor</button>

        </div>

        <?php

          $crearProveedor = new ControladorProveedores();
          $crearProveedor -> ctrCrearProveedor();

        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR USUARIO
======================================-->

<div id="modalEditarProveedor" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" >

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar proveedor</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">


             <!-- ENTRADA PARA EL NIT-->

             <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-file-contract"></i></span> 

                <input type="number" class="form-control input-lg" name="EditarNit" placeholder="Ingresar NIT del proveedor" id="editarNit" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" class="form-control input-lg" id="editarProveedor" name="editarProveedor" placeholder="Escriba el nuevo nombre de proveedor" value="" required>

              </div>

            </div>

             <!-- ENTRADA PARA EL TELEFONO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 

                <input type="text" class="form-control input-lg" id="editarTelefono" name="editarTelefono" placeholder="Escriba el nuevo telefono" value="" required>

              </div>

            </div>

             <!-- ENTRADA PARA LA DIRECCIÓN-->

             <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-map-marked-alt"></i></span> 

                <input type="text" class="form-control input-lg" id="editarDireccion" name="EditarDireccion" placeholder="Ingresar la dirección " required>

              </div>

            </div>

             <!-- ENTRADA PARA EL CORREO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-at"></i></span> 

                <input type="text" class="form-control input-lg" id="editarCorreo" name="editarCorreo" value="" placeholder="Escriba el nuevo correo (opcional)">

              </div>

            </div>


             <!-- ENTRADA PARA LA PAGINA WEB-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-globe"></i></span> 

                <input type="text" class="form-control input-lg" id="editarPaginaWeb" name="editarPaginaWeb" value="" placeholder="Escriba la nueva pagina web (opcional)">

              </div>

            </div>

             <!-- ID DEL PROVEEDOR-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                 

                <input type="hidden" class="form-control input-lg" id="idProveedor_" name="idProveedor_" value="" >

              </div>

            </div>

         </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Modificar usuario</button>

        </div>

     <?php

          $editarProveedor = new ControladorProveedores();
          $editarProveedor  -> ctrEditarProveedor();

        ?> 

      </form>

    </div>

  </div>

</div>

<?php

  $borrarProveedor = new ControladorProveedores();
  $borrarProveedor -> ctrBorrarProveedor();
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/
?> 




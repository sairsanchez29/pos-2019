<?php

/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

require_once "../controladores/proveedores.controlador.php";
require_once "../modelos/proveedores.modelo.php";

class AjaxProveedores{

	/*=============================================
	EDITAR PROVEEDOR (CONTROLADOR DE DATOS Y ENVIA AL MODELO)
	=============================================*/	

	public $idProveedor;

	public function ajaxEditarProveedor()
	{

		$item = "id";
		$valor = $this->idProveedor;

		$respuesta = ControladorProveedores::ctrMostrarProveedores($item, $valor);

		echo json_encode($respuesta);

	}


	/*=============================================
	EDITAR PROVEEDOR (CONTROLADOR DE DATOS Y ENVIA AL MODELO)
	=============================================*/	

	public $idRegistro;
	public function ajaxEditarHistorialProveedor()
	{
		$valor = $this->idRegistro;
		$respuesta = ControladorProveedores::ctrMostrarHistorialProveedor($valor);
		echo json_encode($respuesta);
	}

	

	
}












/*=============================================
EDITAR PROVEEDOR (RECIBE DATO DE JS)
=============================================*/
if(isset($_POST["idProveedor"]))
{

	$editar = new AjaxProveedores();
	$editar ->idProveedor=$_POST["idProveedor"];
	$editar -> ajaxEditarProveedor();

}
/*=============================================
EDITAR HISTORIAL DEL PROVEEDOR (RECIBE DATO DE JS)
=============================================*/

if(isset($_POST["idRegistro"])){

	$editar = new AjaxProveedores();
	$editar ->idRegistro=$_POST["idRegistro"];
	$editar -> ajaxEditarHistorialProveedor();

}




/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/
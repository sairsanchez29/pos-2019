<?php
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

require_once "conexion.php";

class ModeloProductos{

	/*=============================================
	MOSTRAR PRODUCTOS
	=============================================*/

	static public function mdlMostrarProductos($tabla, $item, $valor, $orden){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY id DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else if($orden != "ID_futuro"){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $orden DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}elseif ($orden == "ID_futuro") {
			$ordn = "id";
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $ordn ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();
		}

		$stmt -> close();

		$stmt = null;

	}

/*=============================================
	MOSTRAR PRODUCTOS POR RANGO DE FECHA DE VENTA
	=============================================*/
	static public function mdlMostrarProductosPorRangoFechaDeVenta($tabla, $item, $valor, $orden, $fechaInicial, $fechaFinal)
	{
		if ($fechaInicial != $fechaFinal) 
		{
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinal' ORDER BY $orden DESC");
			$stmt -> execute();
			return $stmt -> fetchAll();
		}
		elseif($fechaFinal == $fechaInicial)
		{
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha like '%$fechaFinal%' ORDER BY $orden DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();
		}
	}


	

	/*=============================================
	REGISTRO DE PRODUCTO
	=============================================*/
	static public function mdlIngresarProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_categoria, codigo, descripcion, imagen, stock, precio_compra, precio_venta) VALUES (:id_categoria, :codigo, :descripcion, :imagen, :stock, :precio_compra, :precio_venta)");


		$stmt->bindParam(":id_categoria", $datos["id_categoria"], PDO::PARAM_INT);
		$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datos["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":imagen", $datos["imagen"], PDO::PARAM_STR);
		$stmt->bindParam(":stock", $datos["stock"], PDO::PARAM_STR);
		$stmt->bindParam(":precio_compra", $datos["precio_compra"], PDO::PARAM_STR);
		$stmt->bindParam(":precio_venta", $datos["precio_venta"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Registrar Stock y proveedor del nuevo producto para estadisticas
	=============================================*/

	static public function mdlIngresarStockYproveedor($tabla, $datos){
		////Modificación para el stock y proveedor del nuevo producto
		
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_producto, id_proveedor, nuevo_stock, precio_compra_por_unidad, lote, fecha_vencimiento) VALUES (:id_producto, :id_proveedor, :stock, :precio_compra, :lote, :fecha_vencimiento)");
		

		$stmt->bindParam(":id_producto", $datos["id_producto"], PDO::PARAM_INT);
		$stmt->bindParam(":id_proveedor", $datos["id_proveedor"], PDO::PARAM_INT);
		$stmt->bindParam(":stock", $datos["stock"], PDO::PARAM_INT);
		$stmt->bindParam(":precio_compra", $datos["precio_compra"], PDO::PARAM_INT);
		$stmt->bindParam(":lote", $datos["fechaFabricacion"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_vencimiento", $datos["fechaVencimiento"], PDO::PARAM_STR);
		
		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;
	}







	/*=============================================
	EDITAR PRODUCTO
	=============================================*/
	static public function mdlEditarProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET id_categoria = :id_categoria, descripcion = :descripcion, imagen = :imagen, stock = :stock, precio_compra = :precio_compra, precio_venta = :precio_venta WHERE codigo = :codigo");

		$stmt->bindParam(":id_categoria", $datos["id_categoria"], PDO::PARAM_INT);
		$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datos["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":imagen", $datos["imagen"], PDO::PARAM_STR);
		$stmt->bindParam(":stock", $datos["stock"], PDO::PARAM_STR);
		$stmt->bindParam(":precio_compra", $datos["precio_compra"], PDO::PARAM_STR);
		$stmt->bindParam(":precio_venta", $datos["precio_venta"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}


	/*=============================================
	EDITAR STOCK PRODUCTO
	=============================================*/
	static public function mdlEditarStockProducto($datos)
	{
		$tabla = "productos";
		//para saber el stock actual debemos buscar al producto relacionado:
		//$producto = $this->mdlMostrarProductos($tabla, "codigo", $datos["cod_producto"], "id");
		// ahora si podemos entrar y saber cual es el stock actual
		//$StockActual = $producto["stock"];
		//restamos el que queremos borrar y que está disponible  y así queda un nuevo stock
		$Stock = $datos["Stock"];
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET stock = (stock - $Stock) WHERE codigo = :codigo");

		$stmt->bindParam(":codigo", $datos["cod_producto"], PDO::PARAM_STR);
		
		

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;
	}
	/*=============================================
	BORRAR PRODUCTO
	=============================================*/

	static public function mdlEliminarProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR PRODUCTO
	=============================================*/

	static public function mdlActualizarProducto($tabla, $item1, $valor1, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE id = :id");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":id", $valor, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR SUMA VENTAS
	=============================================*/	

	static public function mdlMostrarSumaVentas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT SUM(ventas) as total FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;
	}

/*=============================================
	MOSTRAR SUMA VENTAS POR RANGO DE FECHAS
	=============================================*/	
	static public function mdlMostrarSumaVentasPorRango($tabla, $fechaInicial, $fechaFinal)
	{
		$stmt = Conexion::conectar()->prepare("SELECT SUM(ventas) as total FROM $tabla WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinal' ");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;
	}


	/*=============================================
	MOSTRAR HISTORIAL STOCK PRODUCTOS
	=============================================*/	

	static public function mdlMostrarHistorialStock($tabla, $valor){
		$item = "id_producto";
		$ordn = "id";
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_producto = $valor ORDER BY $ordn DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();
		


	}

	/*=============================================
	Inserto la venta de cada paroducto en particular y no en una venta .. 
			la tabla es: "cantidad_de_ventas_por_productos"
	=============================================*/	
	static public function mdlInsertarVentaProducto($CodigoProducto, $DescripcionProducto, $ImagenProducto, $CantidadComprada)
	{
		$tabla = "cantidad_de_ventas_por_productos";
		$fechaActual = date("Y-m-d");

		$stmt_ = Conexion::conectar()->prepare("SELECT * FROM $tabla ");
		$stmt_ -> execute();

		// me doy cuenta si ya existe una venta de este producto en la fecha actual
		$VentasExistentes = 0;
		foreach ($stmt_ as $key => $venta) 
		{
			$fecha = substr($venta["fecha"],0,10);
			if ($fecha == date("Y-m-d") && $venta["codigo"] == $CodigoProducto) 
			{
			  $VentasExistentes++;	
			}
			//echo "<script> alert('".$fecha."'); </script>";
		}

		if ($VentasExistentes == 0) 
		{
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (fecha, ventas, codigo, imagen, descripcion) VALUES (:fecha, :CantidadComprada, :CodigoProducto, :ImagenProducto, :DescripcionProducto)");
			$stmt->bindParam(":CantidadComprada", $CantidadComprada, PDO::PARAM_INT);
			$stmt->bindParam(":CodigoProducto", $CodigoProducto, PDO::PARAM_INT);
			$stmt->bindParam(":ImagenProducto", $ImagenProducto, PDO::PARAM_STR);
			$stmt->bindParam(":DescripcionProducto", $DescripcionProducto, PDO::PARAM_STR);
			$stmt->bindParam(":fecha", $fechaActual, PDO::PARAM_STR);
			$stmt->execute();
		}
		else
		{
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET ventas = (ventas + $CantidadComprada) WHERE codigo = $CodigoProducto");
			$stmt -> execute();
		}

	}



}

/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/



/*=============================================
EDITAR PROVEEDOR
=============================================*/
$(".tablas").on("click", ".btnEditarProveedor", function(){

	var idProveedor = $(this).attr("idProveedor");
	
	var datos = new FormData();
	datos.append("idProveedor", idProveedor);

	$.ajax({

		url:"ajax/proveedores.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			//alert(respuesta["proveedor"]);
			$("#editarProveedor").val(respuesta["proveedor"]);
			$("#editarNit").val(respuesta["nit"]);
			$("#editarDireccion").val(respuesta["direccion"]);
			$("#editarTelefono").val(respuesta["telefono"]);
			$("#editarCorreo").val(respuesta["correo"]);
			$("#editarPaginaWeb").val(respuesta["pagina_web"]);
			$("#idProveedor_").val(respuesta["id"]);
			
		}

	});

})



/*=============================================
ELIMINAR PROVEEDOR
=============================================*/
$(".tablas").on("click", ".btnEliminarProveedor", function(){

  var idProveedor = $(this).attr("idProveedor");
  

  swal({
    title: '¿Está seguro de borrar el proveedor?',
    text: "¡Si no lo está puede cancelar la accíón!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, borrar proveedor!'
  }).then(function(result){

    if(result.value){

    	 window.location = "index.php?ruta=proveedores&idProveedor="+idProveedor;

      

    }

  })

})



/*=============================================
ELIMINAR Historial de stock por PROVEEDOR
=============================================*/
$(".tablas").on("click", ".btnEliminarHistorialProveedor", function(){

  var idStock = $(this).attr("idHistorialRegistroStock");
  var EstadoStock = $(this).attr("EstadoStock");
  var _idProveedor =$(this).attr("IdProveedor");
  var CodProd =$(this).attr("CodProd");
  var _Stock =$(this).attr("_Stock");
  var _page =$(this).attr("page"); // pagina de donde se ejecuta la acción (_proveedor !! _productos)
  
  
  	
	  		if (EstadoStock == "disponible") 
	  		{	
	  			
					 swal({
					 title: '¿Está seguro de borrar este stock?',
					 text: "Este registro de stock está disponible, al borrarlo será restado de la cantidad total del producto. ¡Si no está seguro puede cancelar la acción!",
					 type: 'warning',
					 showCancelButton: true,
					 confirmButtonColor: '#3085d6',
					 cancelButtonColor: '#d33',
					 cancelButtonText: 'Cancelar',
					 confirmButtonText: 'Si, borrar stock!'
					  }).then(function(result){

					    if(result.value)
					    {
							if (_page == "_productos") 
							{
								window.location =  "index.php?ruta=HistorialStock&producto="+CodProd+"&key="+_idProveedor+"&st="+EstadoStock+"&delete="+idStock+"&codProd="+CodProd+"&Sk="+_Stock;
							}
							else if(_page=="_proveedor")
							{
								window.location =  "index.php?ruta=HistorialProveedor&key="+_idProveedor+"&st="+EstadoStock+"&delete="+idStock+"&codProd="+CodProd+"&Sk="+_Stock;
							}
						}

				  	  })


	  		}else if (EstadoStock == "vendido") 
	  		{
	  			swal({
					 title: '¿Está seguro de borrar este stock?',
					 text: "Este registro de stock está vendido, no pasa nada si se borra, solo dejará de mostrarse en esta sección. ¡Si no está seguro puede cancelar la acción!",
					 type: 'warning',
					 showCancelButton: true,
					 confirmButtonColor: '#3085d6',
					 cancelButtonColor: '#d33',
					 cancelButtonText: 'Cancelar',
					 confirmButtonText: 'Si, borrar stock!'
					  }).then(function(result){

					    if(result.value)
					    {
							if (_page == "_productos") 
							{
								window.location =  "index.php?ruta=HistorialStock&producto="+CodProd+"&key="+_idProveedor+"&st="+EstadoStock+"&delete="+idStock+"&codProd="+CodProd+"&Sk="+_Stock;
							}
							else if(_page=="_proveedor") 
							{
								window.location =  "index.php?ruta=HistorialProveedor&key="+_idProveedor+"&st="+EstadoStock+"&delete="+idStock+"&codProd="+CodProd+"&Sk="+_Stock;
							}
						}

				  	  })
	  		}

})






/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/
<?php

/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

class ControladorProveedores{

	

	/*=============================================
	REGISTRO DE PROVEEDOR
	=============================================*/

	static public function ctrCrearProveedor(){

		if(isset($_POST["nuevoProveedor"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoProveedor"])){

				$tabla = "proveedores";

				$datos = array("proveedor" => $_POST["nuevoProveedor"],
					           "telefono" => $_POST["nuevoTelefono"],
					           "correo" =>$_POST["nuevoCorreo"],
					           "pagina_web" => $_POST["nuevaPaginaWeb"],
					           "nit" => $_POST["nuevoNit"],
					           "direccion" => $_POST["nuevaDireccion"]
					         	);

				$respuesta = ModeloProveedores::mdlIngresarProveedor($tabla, $datos);
			
				if($respuesta == "ok"){

					echo '<script>

					swal({

						type: "success",
						title: "¡El proveedor ha sido guardado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){
						
							window.location = "proveedores";

						}

					});
				

					</script>';


				}	


			}else{

				echo '<script>

					swal({

						type: "error",
						title: "¡El proveedor no puede ir vacío o llevar caracteres especiales!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){
						
							window.location = "proveedores";

						}

					});
				

				</script>';

			}


		}


	}

	/*=============================================
	MOSTRAR PROVEEDOR
	=============================================*/

	static public function ctrMostrarProveedores($item, $valor){

		$tabla = "proveedores";

		$respuesta = ModeloProveedores::mdlMostrarProveedores($tabla, $item, $valor);

		return $respuesta;
	}


	static public function ctrMostrarProductosComprados($item, $valor)
	{
		$tabla = "stock_productos_proveedores";

		$respuesta = ModeloProveedores::mdlMostrarProductosComprados($tabla, $item, $valor);

		return $respuesta;
	}



	/*=============================================
	EDITAR PROVEEDOR
	=============================================*/

	static public function ctrEditarProveedor(){

		if(isset($_POST["editarProveedor"])){



			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarProveedor"])){

	
				$tabla = "proveedores";
				$datos = array("proveedor" => $_POST["editarProveedor"],
							   "telefono" => $_POST["editarTelefono"],
							   "correo" => $_POST["editarCorreo"],
							   "pagina_web" => $_POST["editarPaginaWeb"],
							   "idProveedor_" => $_POST["idProveedor_"],
							   "nit" => $_POST["EditarNit"],
							   "direccion" => $_POST["EditarDireccion"]
							   );



				$respuesta = ModeloProveedores::mdlEditarProveedor($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El proveedor ha sido editado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result) {
									if (result.value) {

									window.location = "proveedores";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El nombre no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result) {
							if (result.value) {

							window.location = "proveedores";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	BORRAR USUARIO
	=============================================*/

	static public function ctrBorrarProveedor(){

		if(isset($_GET["idProveedor"])){

			$tabla ="proveedores";
			$datos = $_GET["idProveedor"];

			$respuesta = ModeloProveedores::mdlBorrarProveedor($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "El proveedor ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result) {
								if (result.value) {

								window.location = "proveedores";

								}
							})

				</script>';

			}		

		}

	}



	


/*=============================================
	MOSTRAR HISTORIAL DEL PROVEEDOR
	=============================================*/
	static public function ctrMostrarHistorialProveedor($id_proveedor)
	{
		$tabla = "stock_productos_proveedores";


		$respuesta = ModeloProveedores::mdlMostrarHistorialProveedor($tabla, $id_proveedor);

		return $respuesta;

	}

/*=============================================
	BORRAR HISTORIAL DEL PROVEEDOR
	=============================================*/

	static public function ctrBorrarHistorialProveedor()
	{
		if(isset($_GET["delete"])){

			$tabla ="stock_productos_proveedores";
			$datos = array("id_stock" => $_GET["delete"],
						   "estado" => $_GET["st"],
						   "cod_producto" => $_GET["codProd"], // codigo del producto					
						   "Stock"	=> $_GET["Sk"]
						);

			$respuesta = ModeloProveedores::mdlBorrarHistorialProveedor($tabla, $datos);

			/// restar stock de la cantidad total del producto en caso de que es estado sea "disponible"
			if ($datos["estado"] == "disponible") 
			{
				$r_espuesta = ModeloProductos::mdlEditarStockProducto($datos);
			}else{
				$r_espuesta = "ok";
			}



			if($respuesta == "ok" && $r_espuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "El registro de stock del proveedor ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result) {
								if (result.value) {

								window.location = "index.php?ruta=HistorialProveedor&key='.$_GET["key"].'";

								}
							})

				</script>';

			}		

		}

	}



/*=============================================
	Marcar estado del stock (osea si ya se vendió el stock comprado al proveedor o no)
	=============================================*/

	static public function marcarEstadoStock($id, $NuevoEstado)
	{
		//echo "<script>alert(".$id."); </script>";
		$tabla = "stock_productos_proveedores";
		$respuesta = ModeloProveedores::mdlMarcarEstadoStock($tabla, $id, $NuevoEstado);
		
		return $respuesta;

	}


}









/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/
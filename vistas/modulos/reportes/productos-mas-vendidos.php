<?php

/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

$item = null;
$valor = null;
$orden = "ventas";

if (!isset($_GET["fechaInicial"])) {
  $productos = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);
}else{
  $fechaInicial = $_GET["fechaInicial"];
  $fechaFinal = $_GET["fechaFinal"];
  $_productos = ControladorProductos::ctrMostrarProductosRangoDeFechaDeVentas($item, $valor, $orden, $fechaInicial, $fechaFinal);
  //var_dump($productos);

  //hay que unificar los productos que vienen varias veces(repetidos)
  // si el 999 viene 2 veces las ventas hay que sumarlas 
  $productos = array();
  
  foreach ($_productos as $key => $producto_) 
  {
    $contadorProductos = 0; #cuenta todos los productos en el array productos
    $ProductosIguales = 0; #cuenta las veces que un producto se encuetra repetido
    $cantidadDeVentasDelProducto = 0; // Cantidad final de ventas del producto
    foreach ($productos as $key => $value) # a los que ya estan en el array
    {
      if ($value["codigo"] == $producto_["codigo"]) 
      {
        $ProductosIguales++;
        $cantidadDeVentasDelProducto = $value["ventas"] + $producto_["ventas"];
        # si el producto se encuentra repetido editamos el valor de las ventas del producto ya existente en el array productos
        $productos[$contadorProductos]["ventas"] = ceil($cantidadDeVentasDelProducto);
      }
      $contadorProductos++;
    }



    if ($ProductosIguales == 0)  # si el producto no se encuentra repetido lo agregamos nuevo
    {
      $productos[] = array( "codigo" => $producto_["codigo"],
                          "descripcion" => $producto_["descripcion"],
                         "ventas"=> $producto_["ventas"],
                        "imagen" => $producto_["imagen"],
                        "Programador" => "SairSanchez www.array.com.co");
    
    }
   


    
  }

  //var_dump($productos);

}
 

$colores = array("red","green","yellow","aqua","purple","blue","cyan","magenta","orange","gold");

if (isset($_GET["fechaInicial"])) 
{
    $totalVentas = ControladorProductos::ctrMostrarSumaVentasPorRango($_GET["fechaInicial"], $_GET["fechaFinal"]);
}
else
{
  $totalVentas = ControladorProductos::ctrMostrarSumaVentas();
}



?>

<!--=====================================
PRODUCTOS MÁS VENDIDOS
======================================-->

<div class="box box-default">
	
	<div class="box-header with-border">
  
      <h3 class="box-title">Productos más vendidos</h3>

    </div>

	<div class="box-body">
    
      	<div class="row">

	        <div class="col-md-7">

	 			<div class="chart-responsive">
	            
	            	<canvas id="pieChart" height="150"></canvas>
	          
	          	</div>

	        </div>

		    <div class="col-md-5">
		      	
		  	 	<ul class="chart-legend clearfix">

		  	 	<?php

			
          
            if (isset($_GET["fechaInicial"])) 
            {
              $TotalProductos = count($productos);
               if ($TotalProductos <= 10 ) 
               {
                  for($i = 0; $i < count($productos); $i++)
                  {
                       echo ' <li><i class="fa fa-circle text-'.$colores[$i].'"></i> '.$productos[$i]["descripcion"].'</li>';
                    
                  }
               }
               else
               {
                      for($i = 0; $i < 10; $i++)
                      {
                         echo ' <li><i class="fa fa-circle text-'.$colores[$i].'"></i> '.$productos[$i]["descripcion"].'</li>';
                      
                      }
                }

            }
            else
            {
                for($i = 0; $i < 10; $i++)
                {
                   echo ' <li><i class="fa fa-circle text-'.$colores[$i].'"></i> '.$productos[$i]["descripcion"].'</li>';
                
                }
            }
        


		  	 	?>


		  	 	</ul>

		    </div>

		</div>

    </div>

    <div class="box-footer no-padding">
    	
		<ul class="nav nav-pills nav-stacked">
			
			 <?php

          	for($i = 0; $i <5; $i++){
			
          		echo '<li>
						 
						 <a>

						 <img src="'.$productos[$i]["imagen"].'" class="img-thumbnail" width="60px" style="margin-right:10px"> 
						 '.$productos[$i]["descripcion"].'

						 <span class="pull-right text-'.$colores[$i].'">   
						 '.number_format($productos[$i]["ventas"]*100/$totalVentas["total"]).'%
						 </span>
							
						 </a>

      				</li>';

			}

			?>


		</ul>

    </div>

</div>

<script>
	

  // -------------
  // - PIE CHART -
  // -------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
  var pieChart       = new Chart(pieChartCanvas);
  var PieData        = [

  <?php

  if (isset($_GET["fechaInicial"])) 
  {
      if ($TotalProductos <= 10) 
      {
          for($i = 0; $i < count($productos); $i++)
        {

          echo "{
            value    : ".$productos[$i]["ventas"].",
            color    : '".$colores[$i]."',
            highlight: '".$colores[$i]."',
            label    : '".$productos[$i]["descripcion"]."'
          },";

        }
      }
      else
      {
         for($i = 0; $i < 10; $i++)
        {

          echo "{
            value    : ".$productos[$i]["ventas"].",
            color    : '".$colores[$i]."',
            highlight: '".$colores[$i]."',
            label    : '".$productos[$i]["descripcion"]."'
          },";

        }
      }
  }
  else
  {
    for($i = 0; $i < 10; $i++)
      {

        echo "{
          value    : ".$productos[$i]["ventas"].",
          color    : '".$colores[$i]."',
          highlight: '".$colores[$i]."',
          label    : '".$productos[$i]["descripcion"]."'
        },";

      }
  }
    
   ?>
  ];
  var pieOptions     = {
    // Boolean - Whether we should show a stroke on each segment
    segmentShowStroke    : true,
    // String - The colour of each segment stroke
    segmentStrokeColor   : '#fff',
    // Number - The width of each segment stroke
    segmentStrokeWidth   : 1,
    // Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    // Number - Amount of animation steps
    animationSteps       : 100,
    // String - Animation easing effect
    animationEasing      : 'easeOutBounce',
    // Boolean - Whether we animate the rotation of the Doughnut
    animateRotate        : false,
    // Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale         : false,
    // Boolean - whether to make the chart responsive to window resizing
    responsive           : true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio  : false,
    // String - A legend template
    legendTemplate       : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
    // String - A tooltip template
    tooltipTemplate      : '<%=value %> <%=label%>'
  };
  // Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
  // -----------------
  // - END PIE CHART -
  // -----------------


</script>


<?php 
  /*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/
 ?>
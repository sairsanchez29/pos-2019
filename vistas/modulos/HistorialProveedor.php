<?php
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

if($_SESSION["perfil"] == "Vendedor")
{
  echo '<script>window.location = "inicio";</script>';
	return;
}

		$item = "id" ;
    	$valor = $_GET["key"];
  		$proveedor = ControladorProveedores::ctrMostrarProveedores($item, $valor);
  		

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Historial de stock por proveedor  
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="./"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="">Administrar proveedores</li>
      <li class="active">Historial del proveedor</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <button class="btn btn-info">
          
          <?php echo "Proveedor: ".  $proveedor["proveedor"]; ?>

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Producto</th>
           <th>Stock</th>
           <th>Precio de compra</th>
           <th>Fecha de registro</th>
           <th>Lote</th>
           <th>Fecha de vencimiento</th>
           <th>Estado de venta</th>
           <th>Acciones</th>
         </tr> 

        </thead>

        <tbody>
          
        
		<?php 
			$proveedor = ControladorProveedores::ctrMostrarHistorialProveedor($_GET["key"]);
	   $redirect = false;
			foreach ($proveedor as $key => $value) 
			{
				$item = "codigo";
				$valor = $value["id_producto"];
        $orden = "id";
				$producto = ControladorProductos::ctrMostrarProductos($item, $valor, $orden) ;

        ///saber si un registro de stock está disponible o ya se venió dicha cantidad
        $redirect = false; ///para recargar la pagiina

      if (!isset($_GET["nR"]))  // si no se ha recargado la pagina entonces haga las consultas:
      {
         if ($value["estado"] != "vendido"  ) 
       {
           $CantidadStock = $producto["stock"]; /// total stock del producto

         if ($CantidadStock == $value["nuevo_stock"] || $CantidadStock > $value["nuevo_stock"] ) {
           $Disponible = ControladorProveedores::marcarEstadoStock($value["id"], "disponible");
           $redirect = true;
         }else if ($CantidadStock < $value["nuevo_stock"] )
         {
          $vendido = ControladorProveedores::marcarEstadoStock($value["id"], "vendido");
          $redirect = true;
         }
       }
      }

       if ($value["estado"] == "vendido") {
         $classBtn = "btn btn-warning";
       }else if ($value["estado"] == "disponible")
       {
        $classBtn = "btn btn-success";
      }else{
        $classBtn = "btn btn-desault";
      }

## verificar fecha de vencimiento: 
      $fechaActual=date("Y-m-d");
      
      
      if ($fechaActual >= $value["fecha_vencimiento"] && $value["fecha_vencimiento"] != "0000-00-00") 
      {
        $colorIndicativo =  "orange"; //indicativo vencido
        $value_fecha = $value["fecha_vencimiento"];
        
      }
      else if($fechaActual < $value["fecha_vencimiento"] )
      {
        $colorIndicativo= "blue"; //indicativo no vencido
        $value_fecha = $value["fecha_vencimiento"];
        
      }
      else if ($value["fecha_vencimiento"] == "0000-00-00") 
      {
        $colorIndicativo = "black"; //indicativo no presenta fecha de vencimiento
        $value_fecha = "No registra";
        
      }

      if ($value["lote"] == "0000-00-00") {
        $value_lote = "no registra";
        $colorIlote = "black";
      }else
      {
        $value_lote = $value["lote"];
          $colorIlote = "blue";      
      }

      $page = "_proveedor";
				echo '   
					
			         <tr>
			         	<td>'.($key+1).'</td>
			         	<td>'.$producto["codigo"].'-'.$producto["descripcion"].'</td>
			         	<td>'.$value["nuevo_stock"].'</td>
			         	<td>'.$value["precio_compra_por_unidad"].'</td>
			         	<td>'.$value["fecha"].'</td>
                <td> <b style="color: '.$colorIlote.'">'.$value_lote.'</b></td>
                <td> <b style="color:'.$colorIndicativo.'">'.$value_fecha.' </b></td>
                <td><button class = "'.$classBtn.'">'.$value["estado"].'</button></td>
                <td>
                   
                        
                     
                      <button class="btn btn-danger btnEliminarHistorialProveedor" idHistorialRegistroStock="'.$value["id"].'" EstadoStock="'.$value["estado"].'"   IdProveedor="'.$_GET["key"].'"  CodProd="'.$producto["codigo"].'" _Stock="'.$value["nuevo_stock"].'"  page="'.$page.'" ><i class="fa fa-times"></i></button>

                   
                </td>
			         </tr>

				';
			}
      if ($redirect == true && !isset($_GET["delete"])) {
        echo "<script> window.location = 'index.php?ruta=HistorialProveedor&key=".$_GET['key']."&nR'; </script>";
      }
		 ?>
          


        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>






<?php   

  
    $EliminarStock = ControladorProveedores::ctrBorrarHistorialProveedor();
  
 ?>
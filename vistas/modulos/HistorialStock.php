<?php
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

if($_SESSION["perfil"] == "Vendedor")
{
  echo '<script>window.location = "inicio";</script>';
	return;
}

		$item = "codigo" ;
    	$valor = $_GET["producto"];
    	$orden = "id";

  		$producto = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);
  		

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Historial de Stock del producto <i class="fa fa-barcode"></i> <?php echo $_GET["producto"]; ?>
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="./"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="">Administrar productos</li>
      <li class="active">Historial de Stock</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <button class="btn btn-warning">
          
          <?php echo "Descripción del producto: ".  $producto["descripcion"]; ?>

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Proveedor</th>
           <th>Stock</th>
           <th>Precio de compra</th>
           <th>Fecha de registro</th>
           <th>Lote</th>
           <th>Fecha de vencimiento</th>
           <th>Estado de venta</th>
          <th>acciones</th> 
         </tr> 

        </thead>

        <tbody>
          
        
		<?php 
			$productos = ControladorProductos::ctrMostrarHistorialStock($_GET["producto"]);
			



			foreach ($productos as $key => $value) 
			{
				$item = "id";
				$valor = $value["id_proveedor"];
				$proveedor = ControladorProveedores::ctrMostrarProveedores($item, $valor) ; 



        #estado
         if ($value["estado"] == "vendido") 
         {
          $classBtn = "btn btn-warning";
         }
         else if ($value["estado"] == "disponible")
         {
          $classBtn = "btn btn-success";
         }
         else
         {
            $classBtn = "btn btn-desault";
         }




        ## verificar fecha de vencimiento: 
      $fechaActual=date("Y-m-d");
      
      
      if ($fechaActual >= $value["fecha_vencimiento"] && $value["fecha_vencimiento"] != "0000-00-00") 
      {
        $colorIndicativo =  "orange"; //indicativo vencido
        $value_fecha = $value["fecha_vencimiento"];
        
      }
      else if($fechaActual < $value["fecha_vencimiento"] )
      {
        $colorIndicativo= "blue"; //indicativo no vencido
        $value_fecha = $value["fecha_vencimiento"];
        
      }
      else if ($value["fecha_vencimiento"] == "0000-00-00") 
      {
        $colorIndicativo = "black"; //indicativo no presenta fecha de vencimiento
        $value_fecha = "No registra";
        
      }

      if ($value["lote"] == "0000-00-00") {
        $value_lote = "no registra";
        $colorIlote = "black";
      }else
      {
        $value_lote = $value["lote"];
          $colorIlote = "blue";      
      }






      $page = "_productos";

				echo '   
					
			         <tr>
			         	<td>'.($key+1).'</td>
			         	<td>'.$proveedor["proveedor"].'</td>
			         	<td>'.$value["nuevo_stock"].'</td>
			         	<td>'.$value["precio_compra_por_unidad"].'</td>
			         	<td>'.$value["fecha"].'</td>
                <td> <b style="color:'.$colorIlote.'">'.$value_lote.'</b></td>
                <td> <b style="color:'.$colorIndicativo.'">'.$value_fecha.'</b></td>
                <td><button class = "'.$classBtn.'">'.$value["estado"].'</button></td>

                <td>
                   
                        
                     
                      <button class="btn btn-danger btnEliminarHistorialProveedor" idHistorialRegistroStock="'.$value["id"].'" EstadoStock="'.$value["estado"].'"   IdProveedor="'.$value["id_proveedor"].'"  CodProd="'.$value["id_producto"].'" _Stock="'.$value["nuevo_stock"].'" page="'.$page.'" ><i class="fa fa-times"></i></button>

                   
                </td>
			         </tr>

				';
			}
		 ?>
          


        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>


<?php   

  
    $EliminarStock = ControladorProductos::ctrBorrarHistorialStock();
  
 ?>




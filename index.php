<?php
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

require_once "extensiones/carbon/vendor/autoload.php";
/*carbon inicial para configurar zona horaria COLOMBIA*/

use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setlocale('es');
$fechaActual=Carbon::now()->toDateTimeString();


require_once "controladores/plantilla.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/categorias.controlador.php";
require_once "controladores/productos.controlador.php";
require_once "controladores/clientes.controlador.php";
require_once "controladores/ventas.controlador.php";
require_once "controladores/proveedores.controlador.php";

require_once "modelos/usuarios.modelo.php";
require_once "modelos/categorias.modelo.php";
require_once "modelos/productos.modelo.php";
require_once "modelos/clientes.modelo.php";
require_once "modelos/ventas.modelo.php";
require_once "modelos/proveedores.modelo.php";
require_once "extensiones/vendor/autoload.php";



$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();


// cerrar sesión
  if (isset($_GET["destroy"])) {
    $destroy = new ControladorUsuarios();
    $destroy -> DestroySession();
  }


  /*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/